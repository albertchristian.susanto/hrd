
@extends('layouts.user')
@section('content')


<div class="row clearfix">
	<div class="col col-sm-12">
		<div class="card" style="margin-bottom: -2px!important">
			<div class="header bg-blue">
				<h2>Histori Pengobatan</h2>
				<a href="{{ url('pengobatan/create') }}" class="btn bg-green" style="margin-top: 10px;">
					<i class="material-icons">add</i> Tambah
				</a>
				<a class="btn bg-orange pull-right" style="margin-top: 10px;" href="{{ url('home') }}">
					<i class="material-icons">arrow_back_ios</i> Kembali
				</a>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="body">
						<div class="table-responsive">
							<table class="table table-striped table-hover dataTable" id="datatbl">
								<thead>
									<tr>
										<th width="5px">No.</th>
										<th width="120px">No. Pengajuan</th>
										<th>Tgl</th>
										<th>Tipe</th>
										<th>Hubungan</th>
										<th>Lokasi</th>
										<th>Periode</th>
										<th width="20px">Status</th>
										<th width="10px">Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('script')
<script type="text/javascript">

	$(document).ready(function() {
		var url = '{{ route("api.historiobat") }}';
		$.fn.dataTable.ext.errMode = 'throw';
		var table = $('#datatbl').DataTable( {
			"processing": true,
			"serverSide": true,
			"searching": true,
			"order": [[ 2, "desc" ]],
			"ordering": true,
			"ajax" : {
				"url" : url,
			},
			"columns": [
			{"data" : "no"},
			{"data" : "NoTrans"},
			{"data" : "TglTrans"},
			{"data" : "Tipe"},
			{"data" : "RelasiPegawai"},
			{"data" : "Lokasi"},
			{"data" : "Periode"},
			{"data" : "status"},
			{"data": "action"}],
			"rowCallback": function(row, data, index){
				console.log(data.status);
				if(data.status == 'Menunggu'){
					$('td:eq(7)', row).css('background-color', 'orange',);
					$('td:eq(7)', row).css('color', 'white',);
					$('td:eq(7)', row).css('text-align', 'center',);
				}else if(data.status == 'Disetujui'){
					$('td:eq(7)', row).css('background-color', '#5aa315',);
					$('td:eq(7)', row).css('color', 'white',);
					$('td:eq(7)', row).css('text-align', 'center',);
				}
			},
			"pageLength" : 30,
			"responsive" : true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],


		});

		function loadData(){
			table.ajax.url(url).load();
		}

		$(document).on('click', '.button', function (e) {
			e.preventDefault();
			var id = $(this).data('id');
			var token = $("meta[name='csrf-token']").attr("content");
			var url =
			swal({
				title: "Apakah anda yakin?",
				text: "Setelah data dihapus data tidak akan bisa dikembalikan!",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.ajax({
						type: "DELETE",
						url: "{{ url('pengobatan') }}/" + id,
						data: {
							"id": id,
							"_token": token,
						},
						success: function (data) {
							console.log(data);
							if(data.msg == 'success'){
								swal("Data Hak Cuti dengan ID : " + id + " telah dihapus", {
									icon: "success",
								});
								loadData();
							}else{
								swal("Data gagal di hapus, karena tahun tersebut telah ada pengambilan cuti!", {
									icon: "warning",
								});
							}


						}
					});
				}
			});
		});
	});
</script>
@if (\Session::has('error'))
<?php $error = Session::get("error");  ?>
<script type="text/javascript">
	swal('GAGAL : {{ $error }}', {
		icon: "error",
	});
</script>
@endif
@endsection
